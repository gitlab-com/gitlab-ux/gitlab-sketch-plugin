var sketch = require('sketch');
var Settings = require('sketch/settings');
const BrowserWindow = require('sketch-module-web-view');
const FormData = require('sketch-polyfill-fetch/lib/form-data');

export default function (context) {
  //Settings.setSettingForKey('access-token', undefined);
  //Settings.setSettingForKey('user-id', undefined);
  let win = new BrowserWindow({ width: 800, height: 750, frame: true, title: "GitLab", backgroundColor: '#ffffff' });
  win.webContents.loadURL(require('./export.html'));
  win.webContents.on('needKey', (key) => {
    if (Settings.settingForKey('access-token')) {
      if (!Settings.settingForKey('user-id')) {
        fetch('https://gitlab.com/api/v4/user', {
          method: 'GET',
          headers: { 'Authorization': 'Bearer ' + Settings.settingForKey('access-token') }
        }).then(response => {
          return response.json()
        }).then(success => {
          Settings.setSettingForKey('user-id', success.id);
          let name;
          const document = sketch.fromNative(context.document);
          let layerArray = new Array();
          document.selectedLayers.forEach(layer => {
            name = layer.name;
            layerArray.push({
              image: sketch.export(layer, { formats: 'png', output: false }),
              name: layer.name
            });
          });
          win.webContents.executeJavaScript(`getImage(${JSON.stringify(layerArray)})`).then(res => {
          })
          win.webContents.executeJavaScript('getKey("' + Settings.settingForKey('access-token') + '", "' + name + '", "' + context.document.fileURL().lastPathComponent() + '", ' + Settings.settingForKey('user-id') + ')').then(res => {
          }).catch(error => {
            console.log(error);
          });
        });
      } else {
        let name;
        const document = sketch.fromNative(context.document);
        let layerArray = new Array();
        document.selectedLayers.forEach(layer => {
          name = layer.name;
          layerArray.push({
            image: sketch.export(layer, { formats: 'png', output: false }),
            name: layer.name
          });
        });
        win.webContents.executeJavaScript(`getImage(${JSON.stringify(layerArray)})`).then(res => {
        })
        win.webContents.executeJavaScript('getKey("' + Settings.settingForKey('access-token') + '", "' + name + '", "' + context.document.fileURL().lastPathComponent() + '", ' + Settings.settingForKey('user-id') + ')').then(res => {
        }).catch(error => {
          console.log(error);
        });
      }
    } else {
      win.webContents.loadURL('https://gitlab.com/oauth/authorize?client_id=eb2369ae7c96c1c2f9c1fb69a4461185518e127c779040636c73fffc29e061d7&redirect_uri=https://gitlab.com&response_type=token&state=test&scope=api+read_user+profile');
    }
  });

  win.webContents.on('newKey', (key) => {
    win.webContents.loadURL('https://gitlab.com/oauth/authorize?client_id=eb2369ae7c96c1c2f9c1fb69a4461185518e127c779040636c73fffc29e061d7&redirect_uri=https://gitlab.com&response_type=token&state=test&scope=api+read_user+profile');
  });

  win.webContents.on('will-navigate', (event, url) => {
    if (url.substr(0, 7) == 'file://') {
      win.show();
    }
    let parser = getLocation(url);
    if (parser) {
      parser.href = url;
      if (parser.hostname == 'gitlab.com') {
        if (parser.hash.substr(0, 7) == '#access') {
          Settings.setSettingForKey('access-token', parser.hash.replace('#access_token=', '').toLowerCase().replace('&token_type=bearer&state=test', ''));
          fetch('https://gitlab.com/api/v4/user?access_token=' + Settings.settingForKey('access-token'), {
          }).then(response => {
            return response.json()
          }).then(success => {
            Settings.setSettingForKey('user-id', success.id);
            win.destroy();
            win = new BrowserWindow({ width: 800, height: 750, frame: true, title: "GitLab", backgroundColor: '#ffffff' });
            win.loadURL(require('./export.html'), { extraHeaders: 'pragma: no-cache\n' });
            let name;
            const document = sketch.fromNative(context.document);
            let layerArray = new Array();
            document.selectedLayers.forEach(layer => {
              name = layer.name;
              layerArray.push({
                image: sketch.export(layer, { formats: 'png', output: false }),
                name: layer.name
              });
            });
            win.webContents.executeJavaScript(`getImage(${JSON.stringify(layerArray)})`).then(res => {
            })
            win.webContents.executeJavaScript('getKey("' + Settings.settingForKey('access-token') + '", "' + name + '", "' + context.document.fileURL().lastPathComponent() + '", ' + Settings.settingForKey('user-id') + ')').then(res => {
            }).catch(error => {
              console.log(error);
            });
          });
        }
      } else {
        console.log('parser.hostname incorrect or no code');
      }
    }
  });

  win.webContents.on('openTodo', url => {
    NSWorkspace.sharedWorkspace().openURL(NSURL.URLWithString(url))
  });

  win.webContents.on('submitComment', (projectId, issueId, markdown) => {
    exportToComment(projectId, issueId, markdown);
  });

  win.webContents.on('submitDesign', (projectPath, issueId) => {
    exportToDesigns(projectPath, issueId);
  });

  function exportToComment(projectId, issueId, markdown) {
    const document = sketch.fromNative(context.document);
    document.selectedLayers.forEach(layer => {
      let file = sketch.export(layer, { formats: 'png', output: false });
      var form = new FormData();
      form.append('file', {
        fileName: layer.name + '.png',
        mimeType: 'image/png',
        data: file
      });
      fetch('https://gitlab.com/api/v4/projects/' + projectId + '/uploads', {
        method: 'POST',
        headers: { 'Authorization': 'Bearer ' + Settings.settingForKey('access-token'), 'Content-Type': 'multipart/form-data' },
        body: form
      }).then(response => {
        return response.json()
      }).then(success => {
        fetch('https://gitlab.com/api/v4/projects/' + projectId + '/issues/' + issueId + '/notes?body=' + encodeURI(markdown.replace('[' + layer.name + '](/' + layer.name + '.png)', success.markdown.replace('![', '['))), {
          method: 'POST',
          headers: { 'Authorization': 'Bearer ' + Settings.settingForKey('access-token') }
        }).then(response => {
          return response.json()
        }).then(success => {
          win.destroy();
          sketch.UI.message('Comment submitted 🚀');
        }).catch(
          error => console.log(error)
        );
      }).catch(
        error => console.log(error)
      );
    });
  }

  function exportToDesigns(projectPath, issueId) {
    const document = sketch.fromNative(context.document);
    let v = new Array();
    let m = {};
    let fd = new FormData();
    document.selectedLayers.forEach((layer, index) => {
      m[index] = ["variables.somefile." + index];
      v.push(null);
      let file = sketch.export(layer, { formats: 'png', output: false });
      fd.append(index, {
        fileName: layer.name + '.png',
        mimeType: 'image/png',
        data: file
      });
    });
    fd.append('map', JSON.stringify(m));
    let o = {
      query: `mutation uploadDesign($somefile: [Upload!]!) {
      designManagementUpload(input: {projectPath: "${projectPath}", iid: ${issueId}, files: $somefile}) {
        designs {
          id
          filename
        }
      }
    }`,
      variables: {
        "somefile": v
      }
    };
    fd.append('operations', JSON.stringify(o));
    fetch("https://gitlab.com/api/graphql", {
      "headers": {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + Settings.settingForKey('access-token'),
        'Content-type': "multipart/form-data"
      },
      "body": fd,
      "method": "POST"
    }).then(result => {
      return (result.json());
    }).then(result => {
      win.destroy();
      if (document.selectedLayers.length > 1) {
        sketch.UI.message('Designs uploaded 🚀');
      } else {
        sketch.UI.message('Design uploaded 🚀');
      }
    }).catch(error => {
      console.log(error);
    });
  }

  function getLocation(href) {
    var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
    return match && {
      href: href,
      protocol: match[1],
      host: match[2],
      hostname: match[3],
      port: match[4],
      pathname: match[5],
      search: match[6],
      hash: match[7]
    }
  }
}