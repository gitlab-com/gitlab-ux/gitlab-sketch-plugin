# GitLab Sketch Plugin

# ⚠️ DEPRECATED: Get the Official GitLab Sketch Plugin [Here](https://gitlab.com/gitlab-org/gitlab-sketch-plugin)

🖼 Enhance your design team's Sketch+GitLab workflow

### 👉 [Download Plugin](https://gitlab.com/gitlab-com/gitlab-ux/gitlab-sketch-plugin/-/raw/master/gitlab.sketchplugin.zip?inline=false) 👈

The GitLab Sketch plugin enables you to upload your designs directly from Sketch to GitLab.

##### 👋 Want to contribute?

We have exciting plans for the GitLab Sketch plugin. If you are interested in contributing, please familiarise yourself with https://gitlab.com/gitlab-com/gitlab-ux/gitlab-sketch-plugin/-/issues/7#faq and consider your ideas in the context of our vision.

## Getting Started

1. [Download Plugin](https://gitlab.com/gitlab-com/gitlab-ux/gitlab-sketch-plugin/-/raw/master/gitlab.sketchplugin.zip?inline=false)
2. Unzip the downloaded `.zip` file
3. Add the plugin to Sketch by opening the `gitlab.sketchplugin` file

### Usage

See how to upload your designs from Sketch to GitLab on Youtube:

https://www.youtube.com/watch?v=XN3u0ABpiLA

## Development Guide

### Requirements:

- Node.js
- Global installation of [`skpm`](https://github.com/skpm/skpm) (`npm install -g skpm`)
- Sketch application
- [`sketch-dev-tools`](https://github.com/skpm/sketch-dev-tools) Sketch plugin

### Getting started

1. Install dependencies

```bash
npm install
```

2. Build plugin and watch for changes:

```bash
npm run watch
```

3. Add plugin to Sketch by opening the `./gitlab.sketchplugin` file

### Debugging

To view the output of your `console.log`, you have a few different options:

- Use the [`sketch-dev-tools`](https://github.com/skpm/sketch-dev-tools)
- Run `skpm log` in your Terminal, with the optional `-f` argument (`skpm log -f`) which causes `skpm log` to not stop when the end of logs is reached, but rather to wait for additional data to be appended to the input

_This plugin was created using `skpm`. For a detailed explanation on how things work, checkout the [skpm Readme](https://github.com/skpm/skpm/blob/master/README.md)._

